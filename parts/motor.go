package parts

import (
	"errors"
	"io"
)

// Message convention topic:motor_topic@rpm
// Motor Speed Message
type MotorMessage struct {
	Percent uint8
}

// Motor Actor controls motor Speed
type Motor struct {
	io.Reader
	Describe string
	topic    string
}

//  Topic must be globally unique for that MotorAdapter
func NewMotor(des, topic string) Motor {
	return Motor{
		Describe: des,
		topic:    topic,
	}
}

type MotorAdapter struct {
	Motors []Motor
	Topic  string
}

func NewMotorAdapter() MotorAdapter {
	return NewNamedMotorAdapter("motor:")
}

func NewNamedMotorAdapter(topic string) MotorAdapter {
	return MotorAdapter{
		Motors: make([]Motor, 0, 5),
		Topic:  topic,
	}
}

func (ma MotorAdapter) Add(m Motor) error {
	if len(ma.Motors) == cap(ma.Motors) {
		replace := make([]Motor, 0, len(ma.Motors)*2)
		copy(replace, ma.Motors)
	}
	for _, oldM := range ma.Motors {
		if oldM.topic == m.topic {
			return errors.New("topic of motor is clashing with topic: " + oldM.topic)
		}
	}
	ma.Motors[len(ma.Motors)] = m
	return nil
}
