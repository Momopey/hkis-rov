package main

import (
	"fmt"
	"github.com/shipp02/go-rov/srv"
	"time"
)

func main() {
	sr := srv.NewSR(5000)
	printer := srv.PrinterSub{
		SubTopics: "10001",
	}
	sr.Add(&printer)
	start := time.Now().Second()
	for time.Now().Second()-start < 5 {
		sr.Listen()
		fmt.Println("X")
	}
	sr.Kill()
	//buff := make([]byte, 1024)
}
